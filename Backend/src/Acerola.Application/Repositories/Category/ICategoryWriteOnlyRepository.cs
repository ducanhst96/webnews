﻿using Acerola.Domain.Category;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Acerola.Application.Repositories
{
    public interface ICategoryWriteOnlyRepository
    {
        Task Create(Category category);
    }
}
