﻿using Acerola.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acerola.Domain.Category
{
    public class Category : IEntity
    {
        public Guid Id { get; private set; }
        public Name Name { get; private set; }
        private Category() { }

        public Category Load(Guid id, Name name)
        {
            Category category = new Category();
            category.Id = id;
            category.Name = name;
            return category;
        }
    }
}
