﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acerola.Infrastructure.MongoDataAccess.Entities
{
    public class CategoryDocument : BaseModel
    {
        public string Name { get; set; }
        public List<CategoryDocument> Childs { get; set; }
        
    }
}
