﻿using Acerola.Application.Repositories;
using Acerola.Domain.Category;
using Acerola.Infrastructure.DapperDataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Acerola.Infrastructure.MongoDataAccess.Repositories
{
    public class CategoryRepository : ICategoryWriteOnlyRepository
    {
        private readonly Context _context;

        public CategoryRepository(Context context)
        {
            _context = context;
        }

        public async Task Create(Category category)
        {
            
        }
    }
}
