﻿using Acerola.Domain.Category;
using Acerola.Infrastructure.MongoDataAccess.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acerola.Infrastructure.MongoDataAccess.Mapper
{
    public class CategoryMapper : Profile
    {
        public CategoryMapper()
        {
            CreateMap<Category, CategoryDocument>();

        }
    }
}
