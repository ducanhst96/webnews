﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewApp.WebApi
{
    public class MyConfig
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
