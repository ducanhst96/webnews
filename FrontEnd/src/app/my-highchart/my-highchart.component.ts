import { Component, OnInit } from '@angular/core';
import * as HighCharts from 'highcharts';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-my-highchart',
  templateUrl: './my-highchart.component.html',
  styleUrls: ['./my-highchart.component.scss']
})
export class MyHighchartComponent implements OnInit {

  private _data: any;
  public options: HighCharts.Options = {
    chart: {
      renderTo: 'container',
      type: 'columnrange',
      inverted: true
  },
  
  title: {
      text: 'Temperature variation by month'
  },
  
  subtitle: {
      text: 'Observed in Vik i Sogn, Norway, 2009'
  },

  xAxis: {
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
  },
  
  yAxis: {
      title: {
          text: 'Temperature ( °C )'
      }
  },

  tooltip: {
      valueSuffix: '°C'
  },
  
  plotOptions: {
      columnrange: {
          dataLabels: {
              enabled: true,
              formatter: function () {
                  return this.y + '°C';
              },
              y: 0
          }
      }
  },

  legend: {
      enabled: false
  },

  series: [{
      name: 'Temperatures',
      type: 'area',
      data: [
        ({ x: 0, low: 1, high: 9 })
      ]
  }]

};

  constructor(private _httpClient: HttpClient) {
    this._httpClient.get('https://cdn.jsdelivr.net/gh/highcharts/highcharts@v7.0.0/samples/data/range.json')
    .subscribe(x => {
      console.log(x);
      this._data = x
    });
   }

  ngOnInit() {
    
          HighCharts.chart({
              chart: {
                  type: 'arearange',
                  zoomType: 'x',
                  scrollablePlotArea: {
                      minWidth: 600,
                      scrollPositionX: 1
                  },
                  renderTo: 'my-chart'
              },
              title: {
                  text: 'Temperature variation by day'
              },
  
              xAxis: {
                  type: 'datetime',
                  accessibility: {
                      rangeDescription: 'Range: Jan 1st 2017 to Dec 31 2017.'
                  }
              },
  
              yAxis: {
                  title: {
                      text: null
                  }
              },
  
              tooltip: {
                  shared: true,
                  valueSuffix: '°C',
                  xDateFormat: '%A, %b %e'
              },
  
              legend: {
                  enabled: false
              },
  
              series: [{
                  name: 'Temperatures',
                  data: this._data
              }]
  
          });
      }
    }
