import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterClientComponent } from './footer-client/footer-client.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FooterClientComponent
  ],
  exports: [
    FooterClientComponent
  ]
})
export class ComponentClientModule { }
