import { Component, OnInit } from '@angular/core';
import { ContentService } from 'app/_service/content.service';

@Component({
  selector: 'app-client-layout',
  templateUrl: './client-layout.component.html',
  styleUrls: ['./client-layout.component.scss']
})
export class ClientLayoutComponent implements OnInit {
  private isAdmin = false;

  constructor(private _contentService: ContentService) { }

  ngOnInit() {
    this._contentService.removeLinkCss(this.isAdmin);
  }

}
