import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ContentService {
  constructor() {}
  public removeLinkCss(isAdmin: boolean) {
    if (isAdmin) {
      $('link.client').attr('disabled', 'disabled')
      $('script.client').attr('disabled', 'disabled')
      $('link.admin').removeAttr('disabled')
    } else {
      $('link.admin').attr('disabled', 'disabled')
      $('link.client').removeAttr('disabled')
      $('script.client').removeAttr('disabled')
    }
  }
}
