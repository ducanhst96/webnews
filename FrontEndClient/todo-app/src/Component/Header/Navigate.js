import React, { Component } from "react";

class Navigate extends Component {
  render() {
    return (
      <div id="menubar" className="container">
        <nav id="mainmenu" className="navbar-left collapse navbar-collapse">
          <ul className="nav navbar-nav">
            <li className="sports">
              <a href="listing-sports.html">Trang chủ</a>
            </li>
            <li className="health">
              <a href="listing.html">Thể thao Việt Nam</a>
            </li>
            <li className="technology dropdown mega-cat-dropdown">
              <a
                href="javascript:void(0);"
                className="dropdown-toggle"
                data-toggle="dropdown"
              >
                Bóng đá
              </a>
              <div className="dropdown-menu mega-cat-menu">
                <div className="container">
                  <div className="sub-catagory">
                    <h2 className="section-title">Bóng đá</h2>
                    <ul className="list-inline">
                      <li>
                        <a href="listing.html">Trong nước</a>
                      </li>
                      <li>
                        <a href="listing.html">Thế giới</a>
                      </li>
                      <li>
                        <a href="listing.html">Bóng đá nam</a>
                      </li>
                      <li>
                        <a href="listing.html">Bóng đá nữ</a>
                      </li>
                      <li>
                        <a href="listing.html">Champion leage</a>
                      </li>
                      <li>
                        <a href="listing.html">Road to Olympic</a>
                      </li>
                    </ul>
                  </div>
                  <div className="row">
                    <div className="col-sm-3">
                      <div className="post medium-post">
                        <div className="entry-header">
                          <div className="entry-thumbnail">
                            <img
                              className="img-responsive"
                              src="images/post/technology/6.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div className="post-content">
                          <div className="entry-meta">
                            <ul className="list-inline">
                              <li className="publish-date">
                                <a href="#">
                                  <i className="fa fa-clock-o" /> Nov 5, 2015{" "}
                                </a>
                              </li>
                              <li className="views">
                                <a href="#">
                                  <i className="fa fa-eye" />
                                  15k
                                </a>
                              </li>
                              <li className="loves">
                                <a href="#">
                                  <i className="fa fa-heart-o" />
                                  278
                                </a>
                              </li>
                            </ul>
                          </div>
                          <h2 className="entry-title">
                            <a href="news-details.html">
                              Apple may be preparing for new Beats radio
                              stations
                            </a>
                          </h2>
                        </div>
                      </div>
                      {/*/post*/}
                    </div>
                    <div className="col-sm-3">
                      <div className="post medium-post">
                        <div className="entry-header">
                          <div className="entry-thumbnail">
                            <img
                              className="img-responsive"
                              src="images/post/technology/5.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div className="post-content">
                          <div className="entry-meta">
                            <ul className="list-inline">
                              <li className="publish-date">
                                <a href="#">
                                  <i className="fa fa-clock-o" /> Nov 5, 2015{" "}
                                </a>
                              </li>
                              <li className="views">
                                <a href="#">
                                  <i className="fa fa-eye" />
                                  15k
                                </a>
                              </li>
                              <li className="loves">
                                <a href="#">
                                  <i className="fa fa-heart-o" />
                                  278
                                </a>
                              </li>
                            </ul>
                          </div>
                          <h2 className="entry-title">
                            <a href="news-details.html">
                              Why is the media so afraid of Facebook?
                            </a>
                          </h2>
                        </div>
                      </div>
                      {/*/post*/}
                    </div>
                    <div className="col-sm-3">
                      <div className="post medium-post">
                        <div className="entry-header">
                          <div className="entry-thumbnail">
                            <img
                              className="img-responsive"
                              src="images/post/technology/4.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div className="post-content">
                          <div className="entry-meta">
                            <ul className="list-inline">
                              <li className="publish-date">
                                <a href="#">
                                  <i className="fa fa-clock-o" /> Nov 5, 2015{" "}
                                </a>
                              </li>
                              <li className="views">
                                <a href="#">
                                  <i className="fa fa-eye" />
                                  15k
                                </a>
                              </li>
                              <li className="loves">
                                <a href="#">
                                  <i className="fa fa-heart-o" />
                                  278
                                </a>
                              </li>
                            </ul>
                          </div>
                          <h2 className="entry-title">
                            <a href="news-details.html">
                              Samsung Pay will support online shopping
                            </a>
                          </h2>
                        </div>
                      </div>
                      {/*/post*/}
                    </div>
                    <div className="col-sm-3">
                      <div className="post medium-post">
                        <div className="entry-header">
                          <div className="entry-thumbnail">
                            <img
                              className="img-responsive"
                              src="images/post/technology/3.jpg"
                              alt=""
                            />
                          </div>
                        </div>
                        <div className="post-content">
                          <div className="entry-meta">
                            <ul className="list-inline">
                              <li className="publish-date">
                                <a href="#">
                                  <i className="fa fa-clock-o" /> Nov 5, 2015{" "}
                                </a>
                              </li>
                              <li className="views">
                                <a href="#">
                                  <i className="fa fa-eye" />
                                  15k
                                </a>
                              </li>
                              <li className="loves">
                                <a href="#">
                                  <i className="fa fa-heart-o" />
                                  278
                                </a>
                              </li>
                            </ul>
                          </div>
                          <h2 className="entry-title">
                            <a href="news-details.html">
                              The best games for your phone
                            </a>
                          </h2>
                        </div>
                      </div>
                      {/*/post*/}
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li className="health">
              <a href="listing.html">Các môn khác</a>
            </li>
            <li className="health">
              <a href="listing.html">Hậu trường</a>
            </li>
          </ul>
        </nav>
        <div className="searchNlogin">
          <ul>
            <li className="search-icon">
              <i className="fa fa-search" />
            </li>
            <li className="dropdown user-panel">
              <a
                href="javascript:void(0);"
                className="dropdown-toggle"
                data-toggle="dropdown"
              >
                <i className="fa fa-user" />
              </a>
              <div className="dropdown-menu top-user-section">
                <div className="top-user-form">
                  <form id="top-login" role="form">
                    <div className="input-group" id="top-login-username">
                      <span className="input-group-addon">
                        <img src="images/others/user-icon.png" alt="" />
                      </span>
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Username"
                        required
                      />
                    </div>
                    <div className="input-group" id="top-login-password">
                      <span className="input-group-addon">
                        <img src="images/others/password-icon.png" alt="" />
                      </span>
                      <input
                        type="password"
                        className="form-control"
                        placeholder="Password"
                        required
                      />
                    </div>
                    <div>
                      <p className="reset-user">
                        Forgot <a href="#">Password/Username?</a>
                      </p>
                      <button className="btn btn-danger" type="submit">
                        Login
                      </button>
                    </div>
                  </form>
                </div>
                <div className="create-account">
                  <a href="#">Create a New Account</a>
                </div>
              </div>
            </li>
          </ul>
          <div className="search">
            <form role="form">
              <input
                type="text"
                className="search-form"
                autoComplete="off"
                placeholder="Type & Press Enter"
              />
            </form>
          </div>{" "}
          {/*/.search*/}
        </div>
        {/* searchNlogin */}
      </div>
    );
  }
}

export default Navigate;
