import React, { Component } from 'react';
import Navigate from './Navigate';

class Header extends Component {
    render() {
        return (
            <header id="navigation">
            <div className="navbar" role="banner">
              <div className="container">
                <a className="secondary-logo" href="index.html">
                  <img className="img-responsive" src="images/presets/preset1/logo.png" alt="logo" />
                </a>
              </div>
              <div className="topbar">
                <div className="container">
                  <div id="topbar" className="navbar-header">							
                    <a className="navbar-brand" href="index.html">
                      <img className="main-logo img-responsive" src="images/presets/preset1/logo.png" alt="logo" />
                    </a>
                    <div id="topbar-right">
                      <div className="dropdown language-dropdown">						
                        <a data-toggle="dropdown" href="#"><span className="change-text">En</span> <i className="fa fa-angle-down" /></a>
                        <ul className="dropdown-menu language-change">
                          <li><a href="#">EN</a></li>
                          <li><a href="#">FR</a></li>
                          <li><a href="#">GR</a></li>
                          <li><a href="#">ES</a></li>
                        </ul>								
                      </div>
                      <div id="date-time" />
                      <div id="weather" />
                    </div>
                    <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                      <span className="sr-only">Toggle navigation</span>
                      <span className="icon-bar" />
                      <span className="icon-bar" />
                      <span className="icon-bar" />
                    </button>
                  </div> 
                </div> 
              </div> 

            <Navigate />

            </div>
          </header>
        );
    }
}

export default Header;