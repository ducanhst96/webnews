import React, { Component } from 'react';

class TodoItem extends Component {
  createTask = item => {
    return (
      <li key="{item.key}">
        {item.text}
      </li>
    )
  }
    render() {
      const { entries } = this.props;
      const listItem = entries.map(this.createTask);
      return (
        <ul>
          {listItem}
        </ul>
      );
    }
}

export default TodoItem;