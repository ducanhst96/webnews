import React, {Component} from 'react';
import TopMenu from "./Component/Header/Header";

class App extends Component {
  constructor() {
    super();
    this.state = {
      items: [
      ],
    }
  }
  addItem = (item) => {
    const newTask = {
      text: item,
      key: Date.now()
    }
    if (item !== '' || item !== null) {
      const items = [...this.state.items, newTask]
      this.setState({items})
    }
  }
  
  render () {
    return (
      <div>
        <TopMenu />
      </div>
    );
  }
}

export default App;
